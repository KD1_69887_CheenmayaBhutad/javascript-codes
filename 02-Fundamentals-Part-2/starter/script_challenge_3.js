`use strict`;

const markMiller = {
    fullName : `Mark Miller`,
    mass : 78,
    height : 1.69,
    calcBMI : function() {
        this.BMI = this.mass / (this.height ** 2);
        return this.BMI;
    }
};

const johnSmith = {
    fullName : `John Smith`,
    mass : 92,
    height : 1.95,
    calcBMI : function() {
        this.BMI = this.mass / (this.height ** 2);
        return this.BMI;
    }
};

if(markMiller[`calcBMI`]() > johnSmith[`calcBMI`]()) {
    console.log(`Mark's BMI ${markMiller.BMI} is higher than John's ${johnSmith.BMI}`);
} else {
    console.log(`John's BMI ${johnSmith.BMI} is higher than Mark's ${markMiller.BMI}`);
}

