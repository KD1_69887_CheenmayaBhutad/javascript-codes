`use strict`;

function logger() {
    console.log(`Hello World`);
}

logger();
logger(20); //use strict does not show error for this case.

const variableNoParameter = function () {
    console.log(`Inside a function expression`);
}

variableNoParameter();

function describeCountry(country,population,capitalCity) {
    return `${country} has ${population} million people and its capital city is ${capitalCity}`;
}

const finlandDescription = describeCountry("Finland",6,"Helsinki");
const indiaDescription = describeCountry("India",1440,"Delhi");
const usaDescription = describeCountry("United States of America",33,"Washington D.C");

console.log(finlandDescription,"\n",indiaDescription,"\n",usaDescription);

function percentageOfWorld1(population) {
    return (population/7900) * 100;
}

const chinaPercent = percentageOfWorld1(1441);
const finlandPercent = percentageOfWorld1(6);
const usaPercent = percentageOfWorld1(331);

console.log(`China percent : ${chinaPercent}`);
console.log(`Finland percent : ${finlandPercent}`);
console.log(`USA percent : ${usaPercent}`);

const percentageOfWorld2 = function (population) {
    return (population/7900) * 100;
}

// const russiaPercent = percentageOfWorld2(145);
// const mexicoPercent = percentageOfWorld2(128);
// const japanPercent = percentageOfWorld2(124);

// console.log(`Russia Percent : ${russiaPercent}`);
// console.log(`Mexico Percent : ${mexicoPercent}`);
// console.log(`Japan Percent : ${japanPercent}`);

const percentageOfWorld3 = population => (population/7900) * 100;

const russiaPercent = percentageOfWorld3(145);
const mexicoPercent = percentageOfWorld3(128);
const japanPercent = percentageOfWorld3(124);

console.log(`Russia Percent : ${russiaPercent}`);
console.log(`Mexico Percent : ${mexicoPercent}`);
console.log(`Japan Percent : ${japanPercent}`);

const describePopulation = (country,population) => {
    return `${country} has ${population} million people, which is about ${percentageOfWorld1(population)} of the world`
}

const chinaDescribePopulation = describePopulation("China",1441);
const indiaDescribePopulation = describePopulation("India",1440);
const usaDescribePopulation = describePopulation("U.S.A", 331);

console.log(chinaDescribePopulation);
console.log(indiaDescribePopulation);
console.log(usaDescribePopulation);

/* LECTURE: Introduction to Arrays */
const populations = [1441,145,128,124];
const percentages = [percentageOfWorld1(populations[0]), percentageOfWorld1(populations[1]), percentageOfWorld1(populations[2]), percentageOfWorld1(populations[3])];

console.log(populations.length == 4);
console.log(percentages);

/* LECTURE : Basic Array Operations (Methods) */
const neighbours = [`Bangladesh`, `Myanmar`, `China`, `Pakistan`];
neighbours.push(`Utopia`);
console.log(neighbours);
neighbours.pop(`Utopia`);
console.log(neighbours);

if(neighbours.includes(`Germany`) == false) {
    console.log(`Probably not a central European country :D`);
}

if(neighbours.includes(`China`)) {
    let index = neighbours.indexOf(`China`);
    neighbours[index] = `Republic of ${neighbours[index]}`;
    console.log(neighbours);
}

const cheenmaya = {
    firstName : "Cheenmaya",
    lastName : "Bhutad",
    age : 23
};

console.log(cheenmaya);
cheenmaya.age = 25;
console.log(cheenmaya);

/*LECTURE: Introduction to Objects */
const myCountry = {
    country : `India`,
    capital : `Delhi`,
    language : `Hindi`,
    population : 1441,
    neighbours : [`Myanmar`,`Nepal`,`China`,`Pakistan`,`Sri Lanka`],
}

/*LECTURE: Dot vs. Bracket Notation */
console.log(`${myCountry[`country`]} has ${myCountry.population} million ${myCountry[`language`]}-speaking people, ${myCountry.neighbours.length} countries and a capital called ${myCountry[`capital`]}`);

myCountry.population = myCountry.population + 2;
console.log(myCountry);
myCountry[`population`] = myCountry[`population`] - 2;
console.log(myCountry);

/*LECTURE : Object Methods */
myCountry.describe = function() {
    console.log(`${this.country} has ${this.population} million ${this[`language`]}-speaking people, ${this.neighbours.length} countries and a capital called ${this.capital}`);
}

console.log(myCountry);
myCountry.describe();

myCountry.checkIsland = function() {
    this.neighbours.length == 0 ? this.isIsland = true : this.isIsland = false;
}

console.log(myCountry);
myCountry.checkIsland();

/*LECTURE: Iteration: The for Loop*/
for(let voter = 1;voter <= 50;voter++) {
    console.log(`Voter number ${voter} is currently waiting`);
}

/*LECTURE: Looping Arrays, Breaking and Continuing */
const percentages2 = [];
for(let i = 0;i < populations.length;i++) {
    percentages2.push(percentageOfWorld1(populations[i]));
}

console.log(percentages,percentages2);

/*LECTURE: Looping Backwards and Loops in Loops */
const listOfNeighbours = [
    [`Canada`,`Mexico`],
    [`Spain`],
    [`Norway`,`Sweden`,`Russia`]
];

for(let countryArray = 0; countryArray < listOfNeighbours.length; countryArray++) {
    for(let singleCountry = 0;singleCountry < listOfNeighbours[countryArray].length; singleCountry++) {
        console.log(`Neighbour : ${listOfNeighbours[countryArray][singleCountry]}`);
    }
}

/*LECTURE: The while Loop */
const percentages3 = [];
let counter = 0;
while(counter < populations.length) {
    percentages3[counter] = percentageOfWorld1(populations[counter]);
    counter++;
}

console.log(percentages,percentages3);
