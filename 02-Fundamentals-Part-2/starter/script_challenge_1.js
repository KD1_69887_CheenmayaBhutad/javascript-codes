`use strict`

const dolphinsScore1 = Number.parseInt(prompt("Enter dolphins game 1 score : "));
const dolphinsScore2 = Number.parseInt(prompt("Enter dolphins game 2 score : "));
const dolphinsScore3 = Number.parseInt(prompt("Enter dolphins game 3 score : "));
let dolphinsAverageScore;

const koalasScore1 = Number.parseInt(prompt("Enter koalas game 1 score : "));
const koalasScore2 = Number.parseInt(prompt("Enter koalas game 2 score : "));
const koalasScore3 = Number.parseInt(prompt("Enter koalas game 3 score : "));
let koalasAverageScore;

const calcAverage = (score1,score2,score3) => {
    return (score1 + score2 + score3)/3;
}

dolphinsAverageScore = calcAverage(dolphinsScore1,dolphinsScore2,dolphinsScore3);
koalasAverageScore = calcAverage(koalasScore1,koalasScore2,koalasScore3);

console.log(dolphinsAverageScore,koalasAverageScore);

const checkWinner = (avgDolphins,avgKoalas) => {
    if(avgDolphins >= 2 * avgKoalas) {
        console.log(`Dolphins Win (${avgDolphins} vs ${avgKoalas})`);
    } else if(avgKoalas >= 2 * avgDolphins)
        console.log(`Koalas Win (${avgDolphins} vs ${avgKoalas})`);
}

checkWinner(dolphinsAverageScore,koalasAverageScore);
