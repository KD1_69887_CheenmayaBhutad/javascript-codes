/* Coding challenge 3 */

const dolphinsScore1 = 96;
const dolphinsScore2 = 108;
const dolphinsScore3 = 89;

const koalasScore1 = 88;
const koalasScore2 = 91;
const koalasScore3 = 110;

const averageScoreDolphins = (dolphinsScore1 + dolphinsScore2 + dolphinsScore3)/3;
const averageScoreKoalas = (koalasScore1 + koalasScore2 + koalasScore3) / 3;

if(averageScoreDolphins > averageScoreKoalas && averageScoreDolphins >= 100) {
    console.log(`Dolphins win`);
} else if(averageScoreDolphins < averageScoreKoalas && averageScoreKoalas >= 100) {
    console.log(`Koalas win`);
} else if(averageScoreDolphins === averageScoreKoalas && averageScoreDolphins >= 100 && averageScoreKoalas >=100) {
    console.log(`Its a draw`);
} else {
    console.log("No one wins the trophy");
}

