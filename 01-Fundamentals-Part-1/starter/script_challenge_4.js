/* Coding challenge 4 */

const bill = Number(prompt("Enter the bill value : "));

const tip = bill >= 50 && bill <= 300 ? 0.15 * bill : 0.2 * bill;

console.log(`Tip for the bill ${bill} is ${tip} and final amount is ${bill + tip}`);