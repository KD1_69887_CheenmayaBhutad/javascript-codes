/* Lecture : Values and Variables */

const country = "India";
const continent = "Asia";
let population = 140000000;
// let population = 14000000;

console.log(country,continent,population);

/* Lecture : Data types */
const isIsland = false;
let language;

console.log(isIsland,population,country,language);

/* Lecture : let, const and var */

language = "Hindi"
// language = "english";

/* Lecture : Basic operators */
let halfPopulationCount = population/2;
console.log(halfPopulationCount);

population += 1;
console.log(population, halfPopulationCount);

const finlandPopulation = 6000000;

if(finlandPopulation < population) {
    console.log("My country has larger population");
}

const averagePopulation = 33000000;

if(population < averagePopulation) {
    console.log("My Country has less people than average country");
} else {
    console.log("My Country has more people than average country");
}

let description = country + " is  in " + continent + ", and its " + population + " people speak " + language;
//console.log(description);

/* LECTURE: Strings and Template Literals */

description = `${country} is in ${continent}, and its ${population} people speak ${language}`;
console.log(description);

if(population > 33000000) {
    console.log(`${country}'s population is above average`);
} else {
    console.log(`${country}'s population is ${(33000000 - population)/2} below average`);
}

console.log(`
    ${'9' - '5'}
    ${'19' - '13' + '17'}
    ${'19' - '13' + 17}
    ${'123' < 57}
    ${5 + 6 + '4' + 9 - 4 - 2}
`);

//const numNeighbours = Number(prompt("How many neighbour countries does your country have?"));

// if(numNeighbours == 1) {
//     console.log(`Only one border`);
// } else if(numNeighbours > 1) {
//     console.log(`More than 1 border`);
// } else {
//     console.log(`No borders`);
// }

// if(numNeighbours === 1) {
//     console.log(`Only one border`);
// } else if(numNeighbours > 1) {
//     console.log(`More than 1 border`);
// } else {
//     console.log(`No borders`);
// }

if(language === `english` && population < 50000000 && !isIsland) {
    console.log(`Sarah can live in ${country}`);
} else {
    console.log(`${country} does not satisfy the criteria `)
}

let Language;
Language = 'greek';

switch(Language) {
    case "chinese" :
    case "mandarin" :
        console.log(`Most number of native speakers!`);
        break;

    case "spanish" :
        console.log("2nd place in the number of native speakers");
        break;
    
    case "english" :
        console.log("3rd place");
        break;

    case "hindi" :
        console.log(`Number 4`);
        break;
    
    case "arabic" :
        console.log("5th most spoken language");
        break;

    default :
        console.log("Great langauge too :D");
}

console.log(`${country} population is ${population > 33000000 ? `above` : `below`} average`);
