/* Coding Challenge 1 */

const massJohn = Number(prompt("Enter the mass for John : "));
const heightJohn = Number(prompt("Enter the height for John : "));

const massMark = Number(prompt("Enter the mass for Mark : "));
const heightMark = Number(prompt("Enter the height for Mark : "));

const johnBMI = massJohn / (heightJohn ** 2);
const markBMI = massMark / (heightMark ** 2);

const markHigherBMI = johnBMI > markBMI;

if(markHigherBMI)
    console.log(`John's BMI is higher`);
else
    console.log(`Mark's BMI is higher`);